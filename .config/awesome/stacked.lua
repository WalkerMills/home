local beautiful = require("beautiful")
local tonumber  = tonumber

local stacked = { name = "stacked" }

function stacked.arrange(p)
    -- This layout divides the screen into two tiles; the major area on the
    -- left, and the minor area on the right.  The minor area should be just
    -- large for a terminal.  All windows but the oldest are stacked on top
    -- of one another, covering the rest of the surface area.  It is primarily
    -- intended to be used with a multiplexed terminal, which allows the
    -- smaller window to tile itself. When there is only one client, it is
    -- fullscreened.

    --   +---+---+---+      +---+---+---+      +---+---+---+
    --   |           |      |       |   |      |       |   |
    --   |     1     |  ->  |   1   | 2 | ->   |   1   | 3 |  -> ...
    --   |           |      |       |   |      |       |   |
    --   +---+---+---+      +---+---+---+      +---+---+---+

    -- This should be adjusted such that the far-right column occupies >= 80
    -- characters
    local proportion = .34

    -- A useless gap (like the dwm patch) can be defined with
    -- beautiful.useless_gap_width .
    local useless_gap = tonumber(beautiful.useless_gap_width) or 0
    if useless_gap < 0 then useless_gap = 0 end

    -- A global border can be defined with
    -- beautiful.global_border_width
    local global_border = tonumber(beautiful.global_border_width) or 0
    if global_border < 0 then global_border = 0 end

    local work_area = p.workarea
    local clients = p.clients

    -- Borders are factored in.
    work_area.x = work_area.x + global_border
    work_area.y = work_area.y + global_border
    work_area.height = work_area.height - (global_border * 2)
    work_area.width = work_area.width - (global_border * 2)

    if #clients == 0
    then
        return
    -- If there's only one client, it should be fullscreen
    elseif #clients == 1
    then
        local surface = {
            x = work_area.x,
            y = work_area.y,
            height = work_area.height,
            width = work_area.width
        }
        clients[1]:geometry(surface)
    -- Otherwise, tile the first window with the floating client(s)
    else
        -- Newer clients are stacked atop each other on the left
        local major_surface = {
            x = work_area.x,
            y = work_area.y,
            height = work_area.height,
            width = math.floor(work_area.width * (1 - proportion))
        }
        -- The first client covers the remaining surface
        local minor_surface = {
            x = work_area.x + major_surface.width + useless_gap,
            y = work_area.y,
            height = work_area.height,
            width = work_area.width - major_surface.width - useless_gap
        }

        for i = 1, #clients - 1 do
            clients[i]:geometry(major_surface)
        end
        clients[#clients]:geometry(minor_surface)
    end
end

return stacked
