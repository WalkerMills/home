export PATH=~/bin:$PATH

export CC='gcc'
export CXX='g++'

export EDITOR='vim'

export BITS=`file -L /proc/$$/exe | sed -e 's/.*ELF //' -e 's/-bit.*/ /'`
