# Zsh options

# vi mode
bindkey -v

# History
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=$HISTSIZE
setopt inc_append_history
setopt share_history
setopt hist_ignore_all_dups
setopt hist_find_no_dups

# navigation
setopt autocd
setopt autopushd

setopt interactive_comments

# report background job status immediately
setopt notify

# disable warnings
setopt rmstarsilent
unsetopt nomatch

# Completions
setopt completealiases
setopt extendedglob

autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description '%d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' file-sort name
zstyle ':completion:*' format '%d'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' menu select=0
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/walker/.zshrc'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

bindkey '\eOA' history-substring-search-up
bindkey '\eOB' history-substring-search-down

bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Prompt

autoload -U colors promptinit
colors
promptinit

PROMPT="%{$fg[blue]%}%n%{$reset_color%}@%{$fg[blue]%}%m%{$reset_color%} %{$fg[magenta]%}%d %{$reset_color%}
%# "
RPROMPT="%(?..[%{$fg[yellow]%}%?%{$reset_color%}])"

# ZLE keybindings

# space in normal mode edits current command in vim
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd ' ' edit-command-line

# fix keybindings for special keys

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Aliases & functions

## Development

alias vim='nvim -p'
alias v='vim'
compdef v='vim'

alias g='git'
compdef g='git'
# Must call _git completion once to load the __git_branch_names function
# Otherwise you must use invoke _git once (or use a git tab completion) before
# these completions will work in a new shell
_git
compdef __git_branch_names 'cherry-pick-branch'
compdef __git_branch_names 'restack-branches'
compdef __git_branch_names 'push-branches'
compdef __git_branch_names 'rebase-chain'

#alias gcc='gcc -Wall -Og -g -std=c18 -mtune=generic -march=native -pipe -fstack-protector --param=ssp-buffer-size=4 -Wno-sign-compare'
#alias g++='g++ -Wall -Og -g -std=c++23 -mtune=generic -march=native -pipe -fstack-protector --param=ssp-buffer-size=4 -Wno-sign-compare -Wno-write-strings'
#alias ghc='ghc --make -Wall -O'
#alias javac='javac -Xlint:all'

## Administration

alias reboot='systemctl reboot'
alias poweroff='systemctl poweroff'
alias suspend='systemctl suspend'
alias hibernate='systemctl hibernate'

process () {
    count=`pgrep -c $@`
    if [[ $count -eq 0 ]]
    then
        echo "No matching processes"
    else
        ps wwup `pgrep $@`
    fi
}

alias flush='tmux show-buffer | xclip -i -selection clipboard'

unattached () {
    for i in `tmux list-sessions | grep -v attached | cut -c 1`
    do
        tmux kill-session -t $i
    done
}

## Peripherals

alias single='xrandr --output HDMI-1 --auto --primary --output eDP-1 --off'
alias dual='xrandr --output eDP-1 --primary --auto --output HDMI-1 --auto --right-of eDP-1'

alias xm='xmodmap ~/.Xmodmap'
alias xkb='setxkbmap -layout us'

toggle_xmodmap () {
    # xmodmap is used to swap Caps_Lock and Escape, whose keycodes are 66 & 9,
    # respectively
    keycode=$(xmodmap -pk | awk '/Caps_Lock/ {print $1}')
    if [[ $keycode -eq 66 ]]
    then
        xm
    else
        xkb
    fi
}

## Utilities

alias cp='cp --no-dereference --preserve=all --recursive --verbose'
alias cpr='rsync --partial --progress --archive --checksum --compress --copy-unsafe-links'
alias find='find -O3'
alias grep='grep --color=auto --no-messages --binary-files=without-match'
alias ll='ls -l'
alias ls='ls -C -1 --color=auto --almost-all --human-readable --classify --group-directories-first --dereference-command-line-symlink-to-dir'
alias mkdir='mkdir --parents --verbose'
alias more='less'
alias mv='mv --verbose'
alias rm='rm --force --recursive'
alias shred='shred -fuz'

mktouch () {
		for f in "$@"
		do
			dir=$(dirname $f)
			if [[ ! -d "$dir" ]]
			then
					mkdir -p "$dir"
			fi
			touch $f
		done
}

## Work

alias k='kubectl'
compdef k='kubectl'

alias pod_images='kubectl get pods -o custom-columns=":metadata.name,:spec.containers[0].image" --no-headers'
