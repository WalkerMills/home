" Plugins {{{
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" run ack/ag from vim
" Plugin 'mileszs/ack.vim'
" fuzzy file finder (like Sublime's <c-p>)
Plugin 'ctrlpvim/ctrlp.vim'
" A Vim Plugin for Lively Previewing LaTeX PDF Output
Plugin 'donRaphaco/neotex'
" autocompletion
" Plugin 'Valloric/YouCompleteMe'
" Elixir syntax highlighting
Plugin 'elixir-editors/vim-elixir'
" Aligning (Markdown tables mostly)
Plugin 'junegunn/vim-easy-align'
" Gruvbox color theme
Plugin 'morhetz/gruvbox'
" Go formatting (and more)
Plugin 'fatih/vim-go'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" use ag instead of ack with ack.vim
let g:ackprg = 'ag --vimgrep'
"
" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'   " order matches top to bottom
let g:ctrlp_switch_buffer = 0       " always open files in new buffers 
let g:ctrlp_working_path_mode = 0   " respect changes to pwd
" use ag for CtrlP as well
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" Use XeLaTeX as the default compiler since my resume needs it
let g:neotex_pdflatex_alternative = 'xelatex'
" }}}
" Colors {{{
syntax enable
" Turn on Gruvbox
autocmd vimenter * ++nested colorscheme gruvbox

if (has("nvim"))
"For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif
"For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
if (has("termguicolors"))
set termguicolors
endif

set background=dark
" }}}
" Custom Functions {{{
" toggle between number and relativenumber
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc

function! FormatElixir()
    silent !mix format
    edit
endfunc

function! FormatPython()
    silent !black --line-length 79 %
    edit
endfunc
" }}}
" Custom Keybindings {{{
let mapleader=","       " leader is comma (instead of \)

" - Plugins

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>
" open ack.vim (uses ag instead)
nnoremap <leader>a :Ack

" - Reading

" toggle between absolute & relative numbering
nnoremap <leader>n :call ToggleNumber()<CR>
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR> 

" - Editing

inoremap jj <Esc>

" toggle paste mode (avoid mangling formatting of pasted text)
set pastetoggle=<leader>p 
" paste, re-select & re-yank text in visual mode (restore register contents)
xnoremap p pgv"@=v:register.'y'<CR>
" toggle spell checking
nnoremap <leader>s :setlocal spell!<CR>
nnoremap <leader>e :edit <BAR> redraw<CR>

" Enable very magic regexes by default
nnoremap / /\v
xnoremap / /\v
cnoremap %s/ %smagic/
cnoremap \>s/ \>smagic/

" Insert newlines without entering insert mode
nnoremap O O<Esc>
nnoremap o o<Esc>

" Wrap lines in the current paragraph
nnoremap <leader>g gqip

" - Buffers & Tabs

" To open a new empty buffer
nnoremap <leader>t :enew<CR>
" Open the current buffer in a new tab. Switch the old tab to the next buffer.
nnoremap <leader>T :bnext <BAR> tabedit #<CR>
" Move to the next buffer
nnoremap <C-j> :bnext<CR>
" Move to the previous buffer
nnoremap <C-k> :bprevious<CR>
" Close the current buffer after moving to the previous one
nnoremap <leader>q :bprevious <BAR> bdelete #<CR>
" Close the current tab
nnoremap <leader>Q :tabclose<CR>
" Show all open buffers and their status
nnoremap <leader>l :ls<CR>

" }}}
" UI {{{
set number              " show line numbers
set showcmd             " show command in bottom bar
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " higlight matching parenthesis

augroup SyntaxSettings
	autocmd!
	autocmd BufNewFile,BufRead *.ts set filetype=typescript
	autocmd BufNewFile,BufRead Tiltfile set filetype=python
augroup END

autocmd Filetype * setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype go,make setlocal noexpandtab tabstop=2 shiftwidth=2 softtabstop=2
" }}}
" Search {{{
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" }}}
" Folding {{{
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
set foldmethod=indent   " fold based on indent level
" }}}

" vim:foldmethod=marker:foldlevel=0
